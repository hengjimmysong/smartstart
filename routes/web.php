<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client.home');
});

Route::get('/apply-now', function () {
    return view('client.apply');
});

Route::get('/faq', function () {
    return view('client.faq');
});

Route::get('/previous-winners', function () {
    return view('client.top5');
});

Route::get('/contact-us', function () {
    return view('client.contact');
});

Route::group(['prefix' => 'kh'], function () {
    Route::get('/', function () {
        return view('client-kh.home');
    });

    Route::get('/apply-now', function () {
        return view('client-kh.apply');
    });

    Route::get('/faq', function () {
        return view('client-kh.faq');
    });

    Route::get('/previous-winners', function () {
        return view('client-kh.top5');
    });

    Route::get('/contact-us', function () {
        return view('client-kh.contact');
    });
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });

    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });
    Route::resources(['users' => 'UserController']);
    Route::resources(['contacts' => 'ContactController']);
});

Route::post('/contacts', 'ContactController@store');


Auth::routes(['register' => false]);
