@extends('layouts.main')
@section('header')
<title>Apply Now</title>
@endsection

@section('content')
<div class="apply-banner cover" style="background-image: url('images/apply.jpg')">
  <div class="banner sim green">
    <div class="title">
      Apply Now
      <!-- <p class="subtitle">Get in touch today</p> -->
    </div>
  </div>
</div>

<div class="apply">
  <div class="content">
    <p class="title">So, do you want to be a tech entrepreneur?</p>
    <p class="text">
      SmartStart is a nine-month program that will help turn your ideas into successful startup. <br>
      If you think you have what it takes,
      <span class="highlight" onclick="window.open('https://impacthubphnompenh.typeform.com/to/vPr1As')">apply
        here</span>
      for a life-changing experience!
      <br><br>
      To view the application form beforehand, download
      <a class="highlight" href="/resources/SmartStart_3_Application_Form.docx" download>here</a>.
      We only accept online applications.<br>
      To get help with coming up with the next big tech innovation, use this
      <a class="highlight" href="/resources/SmartStart-Inspiration.pdf" download>simple template</a>.
    </p>
  </div>

  <div class="image" style="background-image: url('images/Apply.png')"></div>
</div>
@endsection
