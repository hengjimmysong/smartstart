@extends('layouts.main')
@section('header')
<title>Home</title>
@endsection

@section('content')
<div class="home">

  <div class="slideshow-ctn">
    <div id="l-arrow" class="arrow l-arrow" onclick="moveSlideshow(-1)"
      style="display: none; background-image: url('images/Backward.png')"></div>
    <div id="r-arrow" class="arrow r-arrow" onclick="moveSlideshow(1)"
      style="display: none; background-image: url('images/Forward.png"></div>
    <div id="index" class="index-ctn index">
      <dot id="0" class="item"></dot>
      <dot id="1" class="item"></dot>
      <dot id="2" class="item"></dot>
      <dot id="3" class="item"></dot>
    </div>

    <div id="slideshow" class="slideshow">
      <div class="slide about">
        <div class="image" style="background-image: url('images/Home.png')"></div>
        <div class="info">
          <p class="title callout green">About SmartStart</p>
          <p class="description">SmartStart is a Young Innovator Program to enable young Cambodian university students
            to launch their own tech startups together with Smart and Impact Hub. The program will offer a unique
            learning platform along with mentorship and financial support.</p>
          <div class="btn-bar">
            <div class="btn get-started" onclick="moveSlideshow(1)">Get Started</div>
            <div class="watch-video btn"
              onclick="window.open('https://www.youtube.com/watch?v=G_jacjlBWHA&feature=youtu.be')">
              <div class="icon" style="background-image: url('images/Watch.png')"></div>
              <p class="label">Watch Video</p>
            </div>
          </div>
        </div>
      </div>

      <div class="slide first-slide">
        <div class="image" style="background-image: url('images/Connect.png')"></div>
        <div class="info">
          <p class="number">1</p>
          <p class="title callout green">Connect</p>
          <p class="description">Throughout the program, you will be exposed to Cambodia’s digital startup ecosystem,
            including experienced mentors, co-working spaces, investors and other technical support.</p>
        </div>
      </div>
      <div class="slide second-slide">
        <div class="image" style="background-image: url('images/Skills.png')"></div>
        <div class="info">
          <p class="number">2</p>
          <p class="title callout green">Skills</p>
          <p class="description">This program consists of business modules taught by experts in the startup sector who
            have trained and mentored successful startups in Cambodia. The Technopreneurship Challenge brings the
            successful teams to another province, a prominent source of tech, creative and business ideas in Cambodia.
          </p>
        </div>
      </div>
      <div class="slide third-slide">
        <div class="image" style="background-image: url('images/Headstart.png')"></div>
        <div class="info">
          <p class="number">3</p>
          <p class="title callout green">A headstart in innovation</p>
          <p class="description">Participating in the entire program will sharpen your capabilities and kick-start your
            journey in developing your very own digital innovative idea. You will also gain access to leading industry
            knowledge and seed funding to begin your own venture.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="stake flip">
    <div class="head callout green-gray">
      <p class="title">What is at Stake?</p>
      <p class="description">The 5 winning teams will receive the following:</p>
    </div>
    <div class="info">
      <div class="item">
        <div class="icon" style="background-image: url('images/Reward.png')"></div>
        <p class="content">5,000 USD</p>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Learning.png')"></div>
        <p class="content">6-month funded incubator program with Impact Hub.</p>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Singapore.png')"></div>
        <p class="content">The team with the best business progress after the incubation period will be awarded a fully
          paid trip to visit the likes of Google, Facebook and Microsoft in Singapore.</p>
      </div>

      <p class="description">In partnership with global co-working space provider Impact Hub Phnom Penh, the winning
        teams will gain access to not only free space but also to an ecosystem of resources, mentorship, inspiration and
        collaboration opportunities.</p>
      <p class="notice">Smart is also ready to further invest in your idea after the full SmartStart cycle is complete!
      </p>
    </div>
  </div>

  <div class="process flip">
    <div class="head callout green-gray">
      <p class="title">The Process</p>
    </div>
    <div class="info">
      <div class="item">
        <div class="icon" style="background-image: url('images/Creative.png')"></div>
        <p class="title">Inspire</p>
        <p class="content">120 shortlisted students participating in a Hackathon Challenge</p>
        <div class="btn more" onclick="openCard('inspire')">Learn More</div>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Enable.png')"></div>
        <p class="title">Enable</p>
        <p class="content">Top 60 students working on biz realization via a Technopreneurship Challenge</p>
        <div class="btn more" onclick="openCard('enable')">Learn More</div>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Grow.png')"></div>
        <p class="title">Grow</p>
        <p class="content">Up to 5 teams incubating and accelerating their projects for 6 months</p>
        <div class="btn more" onclick="openCard('grow')">Learn More</div>
      </div>

      <div id="cards" class="cards" style="display: none;">
        <card id="inspire" class="card">
          <div class="close" onclick="closeCard();"></div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/1.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">1</div> -->
              <p class="content">120 applicants selected from the pool of online applications to participate in a 1-day
                event called Hatch.</p>
            </div>
          </div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/2.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">2</div> -->
              <p class="content">Those identified as team leaders around the vertical of interest are invited to pitch
                their ideas. Teams are formed based on interests and skills.</p>
            </div>
          </div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/3.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">3</div> -->
              <p class="content">Teams are invited to a 2-day Hackathon Challenge where 15 teams will be selected by a
                panel of experts to be part of the 5-day stay-in Technopreneurship Challenge.</p>
            </div>
          </div>
        </card>

        <card id="enable" class="card">
          <div class="close" onclick="closeCard();"></div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/4.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">1</div> -->
              <div class="content">
                An off-site 5-day Technopreneurship Challenge shaped around the fundamentals of ideating and growing a
                digital startup:<br>
                <p class="list">Recognizing, evaluating, and capitalizing on opportunities;</p>
                <p class="list">Understanding the importance of rapid prototyping;</p>
                <p class="list">Market validation and experience all aspects of business creation</p>
              </div>
            </div>
          </div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/5.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">2</div> -->
              <p class="content">Involvement of guest speakers, network with mentors and fellow entrepreneurs with a
                focus on “learning by doing”. ​​​​​​​This includes high-level digital training and exposure to the
                basics of back-end and front-end software tools.</p>
            </div>
          </div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/6.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">3</div> -->
              <p class="content">A pitching event in Phnom Penh will shortlist up to 5 of the most outstanding teams who
                will proceed to the 6-month funded incubation program.</p>
            </div>
          </div>
        </card>

        <card id="grow" class="card">
          <div class="close" onclick="closeCard();"></div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/7.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">1</div> -->
              <p class="content">
                A tailored support scheme with full access to a suite of master classes, 1-on-1 sessions with mentors
                and industry leaders to develop a progress plan.
              </p>
            </div>
          </div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/8.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">2</div> -->
              <p class="content">Regular check-ins to monitor progress and troubleshoot challenges, take part in demo
                days and meet with experts from various companies to get insights.</p>
            </div>
          </div>
          <div class="section">
            <div class="image cover" style="background-image: url('images/9.png')"></div>
            <div class="info">
              <!-- <div class="number callout green small">3</div> -->
              <p class="content">Full membership at an international hub which includes full time use of facilities as
                well as free access to regular events, workshops and business clinics.</p>
            </div>
          </div>
        </card>
      </div>
    </div>

  </div>

  <div class="who-can flip">
    <div class="head callout green-gray">
      <p class="title">What are we <br class="mobile-none"> looking for?</p>
      <!-- <p class="description">All university students with:</p> -->
    </div>

    <div class="info">
      <p class="description">All university students with keen interest and curiosity in innovation, entrepreneurship,
        startups and digital technologies. These students must possess any digital business idea in the following
        verticals:</p>

      <div class="item">
        <div class="icon" style="background-image: url('images/Education.png')"></div>
        <p class="content">Digital Education</p>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Entertainment.png')"></div>
        <p class="content">Digital Content & Entertainment</p>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Trend.png')"></div>
        <p class="content">Other Disruptive Industry Model</p>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Digital-Cash.png')"></div>
        <p class="content">Digital Commerce & Payment</p>
      </div>
    </div>
  </div>

  <div class="who-can flip">
    <div class="head callout green-gray">
      <p class="title">Who can apply?</p>
      <!-- <p class="description">All university students with:</p> -->
    </div>

    <div class="info">
      <p class="description">At the minimum, candidates must have:</p>

      <div class="item">
        <div class="icon" style="background-image: url('images/Conversation.png')"></div>
        <p class="content">High English Proficiency</p>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Dreams.png')"></div>
        <p class="content">Massive Drive & Determination</p>
      </div>

      <div class="item">
        <div class="icon" style="background-image: url('images/Solution.png')"></div>
        <p class="content">Entrepreneurship Skills</p>
      </div>
    </div>
  </div>

  <div class="ready-banner cover" style="background-image: url('images/Are-You-Ready.jpg')">
    <div class="banner sim green">
      <p class="title">
        Are<br>
        You<br>
        Ready to<br>
        Innovate<br>
        with<br>
        Smart?
      </p>
      <p class="description">Apply through our online application form:</p>
      <div class="btn apply-now" onclick="renderPage('apply')">Apply Now</div>
    </div>
  </div>

  <div class="sponsor">
    <p class="title callout yellow small">INNOVATING WITH YOU, INVESTING IN YOU!</p>
    <div class="content">
      <div class="section">
        <!-- <p class="title">Endorsed by:</p> -->
        <div class="logos">
          <div class="logo" style="background-image: url('images/MoEYS.png')"></div>
          <div class="logo" style="background-image: url('images/MPTC.png')"></div>
          <div class="logo CBRD" style="background-image: url('images/CBRD.png')"></div>
        </div>
      </div>
      <div class="section">
        <!-- <p class="title">In partnership with:</p> -->
        <div class="logo" style="background-image: url('images/Impact.png')"></div>
      </div>
      <div class="section">
        <!-- <p class="title">Powered by:</p> -->
        <div class="logo smart" style="background-image: url('images/Smart.png')"></div>
      </div>
    </div>

    <p class="follow">Follow us:</p>
    <div class="icons">
      <div class="btn icon" style="background-image: url('images/Facebook.png')"
        onclick="window.open('https://web.facebook.com/smartforcambodia/')"></div>
      <div class="btn icon" style="background-image: url('images/Insta.png')"
        onclick="window.open('https://www.instagram.com/smartaxiata/')"></div>
      <div class="btn icon" style="background-image: url('images/YouTube.png')"
        onclick="window.open('https://www.youtube.com/user/SmartAxiata')"></div>
    </div>
  </div>
</div>
@endsection
