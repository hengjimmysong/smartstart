<div class="navbar">
  <div class="logo" style="background-image: url('/images/SmartStart.png')" onclick="window.location = '/'">
  </div>

  <div class="menu-web">
  <div class="btn item" onclick="window.location = '/apply-now'">Apply Now</div>
    <div class="btn item" onclick="window.location = '/faq'">FAQ</div>
    <div class="btn item" onclick="window.location = '/previous-winners'">Previous Winners</div>
    <div class="btn item" onclick="window.location = '/contact-us'">Contact Us</div>
    <div class="language">
      <p class="current">EN</p>
      <div class="dropdown">
        <p class="item" onclick="window.location = '/kh/{{Request::path()}}'">KH</p>
      </div>
    </div>
  </div>
</div>

<div class="spacer"></div>

<div class="menu-mobile">
  <div class="btn item" onclick="window.location = '/apply-now'">Apply Now</div>
  <div class="btn item" onclick="window.location = '/faq'">FAQ</div>
  <div class="btn item" onclick="window.location = '/previous-winners'">Previous Winners</div>
  <div class="btn item" onclick="window.location = '/contact-us'">Contact Us</div>
  <div id='language' class="language" onclick="toggleLanguageDropdown()">
    <p class="current">EN</p>
    <div class="dropdown">
      <p class="item" onclick="renderLanguage('khmer')">KH</p>
    </div>
  </div>
</div>
