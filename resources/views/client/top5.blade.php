@extends('layouts.main')
@section('header')
<title>Previous Winners</title>
@endsection

@section('content')
<div class="top5-banner cover" style="background-image: url('images/hackathon.jpg')">
  <div class="banner sim green">
    <div class="title">
      Previous<br>
      Winners
    </div>
  </div>
</div>

<div class="top5">
  <div class="title-section">
    <p class="title">Top 5 Winners</p>
    <div class="buttons">
      <div id="btn-cycle1" class="cycle btn" onclick="showCycle('cycle1')">Cycle 1</div>
      <div id="btn-cycle2" class="cycle btn" onclick="showCycle('cycle2')">Cycle 2</div>
    </div>
  </div>
  <cycle id="cycle1" class="winner-ctn" style="display: none;">
    <div class="winners">
      <div class="winner">
        <div class="logo" style="background-image: url('images/GoSoccer.png')"></div>
        <p class="name">GoSoccer</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/Ligo.png')"></div>
        <p class="name">Ligo</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/Prestige.png')"></div>
        <p class="name">Prestige</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/Propey.png')"></div>
        <p class="name">Propey</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/Spare.png')"></div>
        <p class="name">Spare</p>
      </div>
    </div>

    <div class="pictures">
      <div class="item">
        <div class="image cover" style="background-image: url('images/Final-cycle-1.jpg')"></div>
        <div class="label">Final Pitch</div>
      </div>

      <div class="item">
        <div class="image cover" style="background-image: url('images/Grand-final-cycle-1.jpg')">
        </div>
        <div class="label">Grand Final</div>
      </div>
    </div>
  </cycle>

  <cycle id="cycle2" class="winner-ctn" style="display: none;">
    <div class="winners">
      <div class="winner">
        <div class="logo" style="background-image: url('images/Haystome.png')"></div>
        <p class="name">Haystome</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/HomeX.png')"></div>
        <p class="name">Homex</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/Malis.png')"></div>
        <p class="name">Malis</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/SomJot.png')"></div>
        <p class="name">SomJot</p>
      </div>
      <div class="winner">
        <div class="logo" style="background-image: url('images/Tosrean.png')"></div>
        <p class="name">Tos Rean</p>
      </div>
    </div>

    <div class="pictures">
      <div class="item">
        <div class="image cover" style="background-image: url('images/Final-cycle-2.jpg')"></div>
        <div class="label">Final Pitch</div>
      </div>

      <div class="item">
        <div class="image cover" style="background-image: url('images/Grand-final-cycle-2.jpg')">
        </div>
        <div class="label">Grand Final</div>
      </div>
    </div>
  </cycle>
</div>
@endsection
