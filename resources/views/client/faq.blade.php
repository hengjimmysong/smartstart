@extends('layouts.main')
@section('header')
<title>FAQ</title>
@endsection

@section('content')

<div class="faq-banner cover" style="background-image: url('images/FAQ.jpg')">
  <div class="banner sim green">
    <p class="title">FAQ</p>
  </div>
</div>

<div class="faq">
  <p class="program-title">
    The Program
  </p>

  <div class="item">
    <p class="question">1 What is the objective of this program?</p>
    <p class="answer">SmartStart is a Young Innovator Program aimed at enabling and empowering young Cambodian talents
      to develop their tech and digital innovative ideas with Smart; the goal is to help turn the best concepts into
      actual tech enterprises.</p>
  </div>

  <div class="item">
    <p class="question">2 What are the benefits of attending this program?</p>
    <p class="answer">
      SmartStart offers a unique learning platform along with mentorship and financial support. Through this program,
      you will get:<br><br>
      <span class="highlight">Connected: </span>You will be exposed to various elements of Cambodia’s digital startup
      ecosystem, such as experienced mentors, inspirational speakers, co-working spaces, investors and other technical
      support.<br><br>
      <span class="highlight">Skills: </span>The program consists of business modules taught by experts in their field
      who have trained and mentored successful startups in Cambodia. The Technoprenuership Challenge will bring
      successful teams to a province outside Phnom Penh.<br><br>
      <span class="highlight">Funding: </span>Up to five winning teams will receive 5,000 USD while undergoing a 6-month
      funded incubator program in partnership with a global hub, where they will access not only free space but also an
      array of resources, mentorship, inspiration, and collaboration opportunities. The team with the best progress
      during the Grow Phase will also be awarded with a sponsored Tech Trip to visit the likes of Google, Facebook and
      Microsoft in Singapore. Smart is also ready to further invest in your idea after the full SmartStart cycle is
      completed!
    </p>
  </div>

  <div class="item">
    <p class="question">3 Do I have to bear any cost to participate in this program?</p>
    <p class="answer">Smart will cover all program-related expenses, including meals, refreshments, program materials
      etc. Transportation and accommodation will also be provided for the Technopreneurship Challenge during the Enable
      Phase.</p>
  </div>

  <div class="item">
    <p class="question">4 What is the selection process like?</p>
    <div class="answer">
      <span class="highlight">Inspire Phase:</span><br>
      <p class="list">120 students will be selected from the pool of applications to participate in half-day events
        called Hatch, centered around their vertical of interest to pitch ideas and form their respective teams based on
        interest and skills.</p>
      <p class="list">Teams are invited to a 2-day Hackathon event, including prototyping and demo, which will shortlist
        up to 15 ideas through a pitching event to proceed to the next Phase for further development.</p>

      <span class="highlight">Enable Phase :</span><br>
      <p class="list">An off-site 5-day Technopreneurship Challenge, shaped around the fundamentals of ideating and
        growing a digital startup: recognizing, evaluating, and exploiting opportunities; understanding the importance
        of rapid prototyping; market validation; and experiencing all aspects of business creation. This includes
        high-level digital training and exposure to the basics of back-end and front-end software tools.</p>
      <p class="list">Involvement of guest speakers both local and international, networking with mentors and fellow
        entrepreneurs with a focus on ‘learning by doing’.</p>
      <p class="list">Concludes with a public pitching event in Phnom Penh after the 5-day challenge to select up to 5
        of the most outstanding or promising digital ideas. The respective teams will proceed to the following 6-month
        incubation program.</p>

      <span class="highlight">Grow Phase :</span><br>
      <p class="list">A 6-month funded incubation process and tailored support scheme in partnership with Impact Hub
        Phnom Penh, to turn ideas with the highest potential into actual businesses or products through an ecosystem of
        resources, mentorship, inspiration, and collaboration opportunities with business and tech consultants.</p>
      <p class="list">Besides having regular check-ins to monitor progress and troubleshoot challenges, the teams will
        take part in demo days and meet with experts and mentors from various companies to get insights and understand
        the relevance of their startup.</p>
      <p class="list">Teams will enjoy full membership at Impact Hub Phnom Penh which includes fulltime use of
        facilities as well as free access to regular events, workshops and business clinics.</p>
    </div>
  </div>

  <div class="item">
    <p class="question">5 What are the requirements of participation if I am selected?</p>
    <p class="answer">If you are selected and continue to be shortlisted, you will be required to fully participate in
      all events of this program, carried out in 3 Phases, which include the Hatch, Hackathon, Technoprenuership
      Challenge, and respective pitching events, as outlined above. If your team advances to the Grow phase, you will be
      required to work on your business idea on a part-time basis at Impact Hub Phnom Penh.</p><br>
  </div>

  <p class="program-title">
    The Application
  </p>

  <div class="item">
    <p class="question">When is the application deadline?</p>
    <p class="answer">3<sup>rd</sup> April 2019</p>
  </div>

  <div class="item">
    <p class="question">Who can apply?</p>
    <div class="answer">
      This program is open to all university students with:<br>
      <p class="list">Keen interest and curiosity in innovation, entrepreneurship, startups and digital technologies.
      </p>
      <p class="list">Any digital business idea in the following verticals: Digital Education, Digital Commerce and
        Payment, Digital Entertainment and Content and Other Disruptive Industry Models such as in Healthcare,
        Agriculture and Transportation.</p>
      <p class="list">Digital or entrepreneurship skills that may contribute towards the development of one’s ideas, for
        example: coding, graphic/web design, marketing, writing, sales etc.</p>
      <p class="list">Good English proficiency.</p>
      <p class="list">Strong drive and determination to make his or her dreams come true.</p>
      <p class="list">Commitment to fully participate in all sessions of the program.</p>
      <p class="list">Year 3 and 4 students are strongly encouraged to apply.</p>
    </div>
  </div>

  <div class="item">
    <p class="question">How do I apply for this program?</p>
    <p class="answer">We only accept online applications. To view the online document beforehand, download <a
        class="highlight" href="/resources/SmartStart_3_Application_Form.docx" download>here</a>. Click <span
        class="highlight" onclick="window.open('https://impacthubphnompenh.typeform.com/to/vPr1As')">here</span> to get
      started.</p>
  </div>

  <div class="item">
    <p class="question">Are supporting materials required for the application?</p>
    <p class="answer">You are required to submit a video introduction about yourself (please refer to the application
      form for details regarding the video content). You may upload your video to YouTube and provide the link
      accordingly in the application form.</p>
  </div>

  <div class="item">
    <p class="question">When and how will I be notified of my admission status?</p>
    <p class="answer">You will be informed of your application status by the organizers via email, on or before
      12<sup>th</sup> April, 2019. Successful applicants will be asked to confirm participation upon receiving the
      notification; failure to do so will imply that the applicant has rejected the opportunity to join this program,
      and the position will be offered to other deserving applicants.</p>
  </div>

  <div class="item">
    <p class="question">What are the important dates or milestones that I need to take note of?</p>
    <div class="answer two-column">
      <p class="list half">Application Start Date</p>
      <p class="half">20<sup>th</sup> March 2019</p>
      <p class="list half">Application Deadline</p>
      <p class="half">03<sup>rd</sup> April 2019</p>
      <p class="list half">Announcement of Successful Applicants</p>
      <p class="half">12<sup>th</sup> April 2019</p>
      <p class="list half">Hatch</p>
      <p class="half">20<sup>th</sup> April 2019</p>
      <p class="list half">Hackathon</p>
      <p class="half">27th - 28th April 2019</p>
      <p class="list half">Camp</p>
      <p class="half">11<sup>th</sup> - 16<sup>th</sup> May 2019</p>
      <p class="list half">Final Pitch</p>
      <p class="half">9<sup>th</sup> June 2019</p>
      <p class="list half">Growth Phase</p>
      <p class="half">End June onwards</p>

      <p><br>The organizers reserve the right to amend any of the dates above due to unforeseen circumstances.</p>
    </div>
  </div>

</div>
@endsection
