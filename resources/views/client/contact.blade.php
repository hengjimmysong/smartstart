@extends('layouts.main')
@section('header')
<title>Contact Us</title>
@endsection

@section('content')
<div class="contact-banner cover" style="background-image: url('images/Contact-us.jpg')">
  <div class="banner sim green">
    <div class="title">
      Contact Us
      <p class="subtitle">Get in touch today</p>
    </div>
  </div>
</div>

<div class="contact">
  <div class="contact-info">
    <p class="title callout green">Contact Us</p>

    <div class="info">
      <div class="icon" style="background-image: url('images/Phone.png')"></div>
      <div class="content">
        <p class="title">Phone</p>
        <p class="subtitle">+85510234075</p>
      </div>
    </div>

    <div class="info">
      <div class="icon" style="background-image: url('images/Email.png')"></div>
      <div class="content">
        <p class="title">Email</p>
        <p class="subtitle">smartstart@smart.com.kh</p>
      </div>
    </div>

    <div class="info">
      <div class="icon" style="background-image: url('images/Website.png')"></div>
      <div class="content">
        <p class="title">Website</p>
        <p class="subtitle">smartstart.com.kh</p>
      </div>
    </div>
  </div>

  <form class="form" action="/contacts" method="POST">
    {{csrf_field()}}
    <p class="title callout green">or fill the form</p>

    <input type="text" name="name" placeholder="Name" class="input">
    <input type="text" name="email" placeholder="Email" class="input">
    <input type="text" name="phone" placeholder="Phone Number" class="input">
    <textarea name="message" placeholder="Message" class="input area"></textarea>

    <input type="submit" value="Submit" class="btn submit">
  </form>


  <div class="map-ctn">
    <p class="title callout green">Location</p>
    <div class="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.197852927056!2d104.92135141494555!3d11.537660347835356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310950dbd81ec78d%3A0x8d3ccf621a2b4e1f!2sSmart+Axiata!5e0!3m2!1sen!2skh!4v1552488128259" frameborder="0" style="border:0" allowfullscreen class="frame"></iframe>
      <div class="info"></div>
    </div>
  </div>


  <div class="social">
    <p class="follow">Follow us:</p>
    <div class="icons">
      <div class="btn icon" style="background-image: url('images/Facebook.png')" onclick="window.open('https://web.facebook.com/smartforcambodia/')"></div>
      <div class="btn icon" style="background-image: url('images/Insta.png')" onclick="window.open('https://www.instagram.com/smartaxiata/')"></div>
      <div class="btn icon" style="background-image: url('images/YouTube.png')" onclick="window.open('https://www.youtube.com/user/SmartAxiata')"></div>
    </div>
  </div>
</div>

@endsection
