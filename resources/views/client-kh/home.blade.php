@extends('layouts.main-kh')
@section('header')
<title>ទំព័រដើម</title>
@endsection

@section('content')


<div class="slideshow-ctn">
  <div id="l-arrow-kh" class="arrow l-arrow" onclick="moveSlideshow(-1)"
    style="display: none; background-image: url('/images/Backward.png')"></div>
  <div id="r-arrow-kh" class="arrow r-arrow" onclick="moveSlideshow(1)"
    style="display: none; background-image: url('/images/Forward.png"></div>
  <div id="index-kh" class="index-ctn index">
    <dot id="0" class="item"></dot>
    <dot id="1" class="item"></dot>
    <dot id="2" class="item"></dot>
    <dot id="3" class="item"></dot>
  </div>

  <div id="slideshow-kh" class="slideshow">
    <div class="slide about">
      <div class="image" style="background-image: url('/images/Home.png')"></div>
      <div class="info">
        <p class="title callout green">អំពី SmartStart</p>
        <p class="description">SmartStart គឺជាកម្មវិធីអ្នកច្នៃប្រឌិតវ័យក្មេងមួយដែលមាន
          គោលបំណងផ្តល់ឱកាសឲ្យនិស្សិតកម្ពុជាដែលមានទេពកោសល្យ និងសមត្ថភាព ដើម្បីអភិវឌ្ឍគំនិតច្នៃប្រឌិតអំពីវិស័យ
          បច្ចេកវិទ្យា និង ឌីជីថល របស់ពួកគេ ជាមួយនឹង ក្រុមហ៊ុន Smart និងImpact Hub Phnom Penh ។ SmartStart
          ជាកម្មវិធីតែមួយគត់ ដែលផ្តល់នូវវេទិកាសម្រាប់ សិក្សារៀនសូត្រ និងទទួលបានការជួយ បង្ហាត់បង្ហាញពីអ្នក ជំនាញ
          និងទទួលជំនួយផ្នែកហិរញ្ញវត្ថុទៀតផង។</p>
        <div class="btn-bar">
          <div class="btn get-started" onclick="moveSlideshow(1)">Get Started</div>
          <div class="watch-video btn"
            onclick="window.open('https://www.youtube.com/watch?v=G_jacjlBWHA&feature=youtu.be')">
            <div class="icon" style="background-image: url('/images/Watch.png')"></div>
            <p class="label">មើលវីដេអូរបស់យើង</p>
          </div>
        </div>
      </div>
    </div>

    <div class="slide first-slide">
      <div class="image" style="background-image: url('/images/Connect.png')"></div>
      <div class="info">
        <p class="number">1</p>
        <p class="title callout green">ទំនាក់ទំនង</p>
        <p class="description">តាមរយៈកម្មវិធីនេះ
          អ្នកនឹងត្រូវបាន​ស្វែងយល់ពីប្រព័ន្ធអេកូស៊ីស្ដែមនៃអាជីវកម្មទើបនឹងចាប់ផ្ដើមបែបឌីជីថលក្នុងប្រទេសកម្ពុជា
          រួមទាំងជួបអ្នកណែនាំដែលមានបទពិសោធន៍ កន្លែងធ្វើការរួមគ្នា អ្នកវិនិយោគ និងកាគាំទ្រផ្នែកបច្ចេកទេសផ្សេងទៀត។</p>
      </div>
    </div>
    <div class="slide second-slide">
      <div class="image" style="background-image: url('/images/Skills.png')"></div>
      <div class="info">
        <p class="number">2</p>
        <p class="title callout green">ជំនាញ</p>
        <p class="description">
          កម្មវិធីនេះមានម៉ូឌុលអាជីវកម្មដែលត្រូវបានបង្រៀនដោយអ្នកឯកទេសក្នុងវិស័យអាជីវកម្មទើបនឹងចាប់ផ្ដើម
          និងអ្នកបង្ហាត់បង្ហាញប្រកបដោយជោគជ័យក្នុងការចាប់ផ្តើមអាជីវកម្មនៅកម្ពុជា។ កម្មវិធី Technopreneurship Challenge
          នឹងនាំយកក្រុមដែលទទួលបានជោគជ័យទៅកាន់ខេត្តមួយ
          ដែលជាប្រភពគំនិតច្នៃប្រឌិតបច្ចេកវិទ្យាគំនិតច្នៃប្រឌិតនិងអាជីវកម្មនៅក្នុងប្រទេសកម្ពុជា។</p>
      </div>
    </div>
    <div class="slide third-slide">
      <div class="image" style="background-image: url('/images/Headstart.png')"></div>
      <div class="info">
        <p class="number">3</p>
        <p class="title callout green">ចាប់ផ្ដើមការឆ្នៃប្រឌិត</p>
        <p class="description">
          ការចូលរួមក្នុងកម្មវិធីទាំងមូលនឹងធ្វើអោយសមត្ថភាពអ្នកកាន់តែប្រសើរឡើងនិងចាប់ផ្តើមដំណើររបស់អ្នកក្នុងការអភិវឌ្ឍគំនិតឌីជីថលរបស់អ្នក។
          អ្នកក៏នឹងទទួលបានចំណេះដឹងផ្នែកឧស្សាហកម្មឈានមុខនិងការផ្តល់មូលនិធិដើម្បីចាប់ផ្តើមការបណ្តាក់ទុនផ្ទាល់ខ្លួនរបស់អ្នក។
        </p>
      </div>
    </div>
  </div>
</div>

<div class="stake flip">
  <div class="head callout green-gray">
    <p class="title">អ្វីដែលនៅភាគហ៊ុន?</p>
    <p class="description">៥ ក្រុមដែលឈ្នះនឹងទទួលបាន៖</p>
  </div>
  <div class="info">
    <div class="item">
      <div class="icon" style="background-image: url('/images/Reward.png')"></div>
      <p class="content">ទឹកប្រាក់ ៥ ០០០ ដុល្លារ</p>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Learning.png')"></div>
      <p class="content">កម្មវិធីបណ្តុះបណ្តាលរយៈពេល ៦ ខែ</p>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Singapore.png')"></div>
      <p class="content">ក្រុមដែលមានអាជីវកម្មរីកចំរើនលឿនជាងគេ នឹងត្រូវបានអញ្ជើញឲ្យទៅទស្សនកិច្ចសិក្សានៅ ការិយាល័យ Google,
        Facebook និង Microsoft នៅប្រទេសសឹង្ហបូរីដោយឥតគិតថ្លៃ។</p>
    </div>

    <p class="description">សកម្មភាពទាំងអស់នេះនឹងត្រូវបានធ្វើឡើងក្រោមភាពជាដៃគូជាមួយនឹង បណ្ដាញអ្នកគាំទ្រអន្តរជាតិមួយ
      ដែលនៅទីនោះក្រុមដែលទទួលបាន ជ័យជំនះទាំង ៥ អាចទទួលបានកន្លែងធ្វើការដោយឥតគិតថ្លៃ និងជំនួយ លើធនធានផ្សេងៗរួមមាន
      ការហ្វឹកហាត់ ការលើកទឹកចិត្ត និងឱកាស ដៃគូជាច្រើនទៀតផងដែរ។</p>
    <p class="notice">ក្រោយពេលកម្មវិធីស្មាតស្តាតបានបញ្ចប់ទៅក្រុមហ៊ុនស្មាត
      ក៏អាចបន្តផ្តល់ការវិនិយោគទុនទៅលើគំនិតរបស់អ្នកទៀតផង!</p>
  </div>
</div>

<div class="process flip">
  <div class="head callout green-gray">
    <p class="title">ដំណើរការ</p>
  </div>
  <div class="info">
    <div class="item">
      <div class="icon" style="background-image: url('/images/Creative.png')"></div>
      <p class="title">វគ្គបំផុសគំនិត</p>
      <p class="content">និស្សិតចំនួន ១២០ រូបចាប់ ផ្ដើមបង្កើតគម្រោង ផ្សេងៗតាមរយៈកម្មវិធី Hackathon។</p>
      <div class="btn more" onclick="openCard('inspire')">អានបន្ថែម</div>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Enable.png')"></div>
      <p class="title">វគ្គបណ្តុះសមត្ថភាព</p>
      <p class="content">និស្សិត៦០ រូបធ្វើការដើម្បី បង្កើតគម្រោងអាជីវកម្ម តាមរយៈការប្រកួត សហគ្រិនបច្ចេកវិទ្យា
        (Technopreneur Challenge)</p>
      <div class="btn more" onclick="openCard('enable')">អានបន្ថែម</div>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Grow.png')"></div>
      <p class="title">វគ្គលូតលាស់</p>
      <p class="content">៥ ក្រុមនឹងទទួលបាន ការបណ្តុះបណ្តាល ដើម្បី ពន្លឿនគម្រោងរបស់គេ។</p>
      <div class="btn more" onclick="openCard('grow')">អានបន្ថែម</div>
    </div>

    <div id="cards-kh" class="cards" style="display: none;">
      <card id="inspire-kh" class="card">
        <div class="close" onclick="closeCard();"></div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/1.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">1</div> -->
            <p class="content">យើងនឹងជ្រើសរើសសិស្ស និស្សិត ចំនួន ១២០ រូបពីពាក្យស្នើរសុំ
              ដើម្បីឲ្យមកចូលរួមកម្មវិធីដែលមានរយៈពេលពាក់កណ្តាលថ្ងៃ មានឈ្មោះថា Hatch ។</p>
          </div>
        </div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/2.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">2</div> -->
            <p class="content">បេក្ខជនដែលមានគំនិតអាជីវកម្មជាក់លាក់ នឹងត្រូវបានអញ្ជើញឲ្យធ្វើបទបង្ហាញពីគំនិតរបស់ពួកគេ
              បន្ទាប់មកបេក្ខជននឹងជ្រើសរើសក្រុមដោយផ្អែកទៅលើចំណាប់អារម្មណ៍ និងជំនាញរបស់ពួកគេ។</p>
          </div>
        </div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/3.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">3</div> -->
            <p class="content">ក្រុមនីមួយៗនឹងត្រូវ ចូលរួមកម្មវិធីបង្កើតផលិតផលគម្រូ ដែលមាន រយៈពេល ២ថ្ងៃ ដែលក្នុងនោះពួក
              គេត្រូវធ្វើបទបង្ហាញទៅកាន់គណៈ កម្មការអំពីផលិតផលរបស់ពួកគេ។ អ្នកជំនាញនឹងជ្រើសរើសពី ១០ ទៅ ១៥
              ក្រុមដែលល្អជាងគេដើម្បីបន្ត ទៅវគ្គបន្ទាប់ដែលមានឈ្មោះថា កម្មវិធីបណ្ដុះបណ្ដាលសហគ្រិន បច្ចេកវិទ្យា
              (Technoprenuer Challenge) ដែលនឹងប្រព្រឹត្តឡើង នៅខេត្តបាត់ដំបង ពីថ្ងៃទី ១១ ទៅ ១៥ ខែឧសភា ឆ្នាំ ២០១៧។</p>
          </div>
        </div>
      </card>

      <card id="enable-kh" class="card">
        <div class="close" onclick="closeCard();"></div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/4.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">1</div> -->
            <div class="content">
              កម្មវិធីការប្រកួតសហគ្រិនបច្ចេកវិទ្យា (Technoprenuer Challenge) ជាកម្មវិធីរយៈពេលប្រាំថ្ងៃ
              ធ្វើឡើងនៅខេត្តបាត់ដំបង ដើម្បីបង្រៀនពីការអភិវឌ្ឍន៍គំនិត និងការបង្កើតក្រុមហ៊ុនបច្ចេកវិទ្យាថ្មីដែលរួមមាន៖<br>
              <p class="list">ការ ចេះវិភាគ និងចេះប្រើប្រាស់ឱកាស</p>
              <p class="list">យល់ដឹងពីសារៈសំខាន់នៃការបង្កើតផលិតផលគម្រូឲ្យបានរហ័ស</p>
              <p class="list">យល់ដឹងសុពលភាពទីផ្សារ និងបទពិសោធន៍គ្រប់ផ្នែកនៃការបង្កើតជំនួញមួយ។</p>
            </div>
          </div>
        </div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/5.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">2</div> -->
            <p class="content">ការចូលរួមបង្ហាត់បង្រៀនពីសំណាក់វាគ្មិនពីក្នុង និងក្រៅប្រទេស
              ការបង្កើតទំនាក់ទំនងជាមួយអ្នកមានបទពិសោធន៍ និង សហគ្រិនផ្សេងៗ ដោយផ្តោតសំខាន់ទៅលើ ការរៀនតាមរយៈការធ្វើ
              នេះរួមមាន ការហ្វឹកហាត់ឌីជីថលកម្រិតខ្ពស់ និងការរៀនសូត្រពីភាសាសាមញ្ញក្នុងការបង្កើតភាសាកុំព្យូទ័រ
              សម្រាប់ទាំងផ្នែកខាងក្នុង និងសម្បកក្រៅនៃសហ្វវែកុំព្យូទ័រមួយ។</p>
          </div>
        </div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/6.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">3</div> -->
            <p class="content">វគ្គនេះនឹងបញ្ចប់ជាមួយនឹងកម្មវិធីធ្វើបទបង្ហាញជាសាធារណៈមួយនៅទីក្រុងភ្នំពេញ
              ដើម្បីជ្រើសរើសក្រុមចំនួន ៥ ក្រុមដែលមានគំនិតល្អជាងគេ។ ក្រុមទទួលបានជ័យជំនះទាំង៥
              នេះនឹងបន្តចូលទៅក្នុងកម្មវិធីបណ្តុះបណ្តាលរយៈពេល ៦ ខែបន្តទៀត។</p>
          </div>
        </div>
      </card>

      <card id="grow-kh" class="card">
        <div class="close" onclick="closeCard();"></div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/7.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">1</div> -->
            <p class="content">
              កម្មវិធីបណ្តុះបណ្តាលឥតគិតថ្លៃ និងការគាំទ្ររយៈពេល ៦ ខែ ដែលត្រូវបានធ្វើឡើងតាមរយៈភាពជាដៃគូជាមួយ global hub
              ក្នុងគោលបំណងប្រែគំនិតដែលមានសក្តានុពលល្អជាងគេ ឲ្យក្លាយទៅជាជំនួញ ឬផលិតផលពិតប្រាកដមួយ
              តាមរយៈការប្រើប្រាស់បណ្ដាញធនធាន ការហ្វឹកហាត់ ការលើកទឹកចិត្ត និងឱកាសធ្វើជាដៃគូជាមួយនឹង ជំនួញ
              និងអ្នកផ្តល់ប្រឹក្សាផ្សេងៗ។
            </p>
          </div>
        </div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/8.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">2</div> -->
            <p class="content">ក្រៅពីធ្វើការពិនិត្យជាទៀតទាត់ដើម្បីតាមដានការលូតលាស់ និងដោះស្រាយបញ្ហាផ្សេងៗ ក្រុមនឹងទទួល
              ឱកាសចូលរួមក្នុងថ្ងៃបង្ហាញផលិតផល និងអាជីវកម្មរបស់ពួកគេ ព្រមទាំងអាច ជួបពិភាក្សាជាមួយនឹងអ្នកជំនាញ
              និងអ្នកមានបទពិសោធន៍ពីក្រុមហ៊ុនផ្សេងៗដើម្បីទទួលបានពត៌មានទាក់ទងជាមួយនឹងអាជីវកម្ម របស់ពួកគេផងដែរ។</p>
          </div>
        </div>
        <div class="section">
          <div class="image cover" style="background-image: url('/images/9.png')"></div>
          <div class="info">
            <!-- <div class="number callout green small">3</div> -->
            <p class="content">ក្លាយជាសមាជិកដោយឥតគិតថ្លៃនៅក្នុងការិយាល័យអន្តរជាតិមួយ ដែលក្នុងនោះអ្នកអាចប្រើប្រាស់សម្ភារៈ
              គ្រប់បែបយ៉ាង ក៏ដូចជាអាចចូលរួមកម្មវិធី សិក្ខាសាលា ទាក់ទងនឹងពត៌មាន អំពីជំនួញផ្សេងៗផងដែរ</p>
          </div>
        </div>
      </card>
    </div>
  </div>

</div>

<div class="who-can flip">
  <div class="head callout green-gray">
    <p class="title">តើពួកយើងស្វែងរកបេក្ខជនបែបណា?​</p>
    <!-- <p class="description">All university students with:</p> -->
  </div>

  <div class="info">
    <p class="description">គ្រប់និស្សិត ដែលមានចំណាប់អារម្មណ៍ និងចង់ស្វែងយល់ពីការច្នៃប្រឌិត សហគ្រិនភាព ការបង្កើតជំនួញថ្មី
      និងបច្ចេកវិទ្យាឌីជីថល។ និស្សិតទាំងនេះត្រូវមានគំនិតជំនួញឌីជីថលដែលមាននៅក្នុងប្រភេទ៖</p>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Education.png')"></div>
      <p class="content">ការអប់រំតាមរយៈឌីជីថល</p>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Entertainment.png')"></div>
      <p class="content">ការកំសាន្ត និងពត៌មានឌីជីថល</p>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Trend.png')"></div>
      <p class="content">វិស័យផ្សេងទៀត</p>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Digital-Cash.png')"></div>
      <p class="content">ពាណិជ្ជកម្ម និងការបង់ប្រាក់បែបឌីជីថល</p>
    </div>
  </div>
</div>

<div class="who-can flip">
  <div class="head callout green-gray">
    <p class="title">តើនរណាខ្លះអាចដាក់ពាក្យចូលរួមបាន?</p>
    <!-- <p class="description">All university students with:</p> -->
  </div>

  <div class="info">
    <p class="description">យ៉ាងហោចណាស់ បេក្ខជនត្រូវមាន៖</p>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Conversation.png')"></div>
      <p class="content">ចំនេះដឹងអង់គ្លេសល្អ</p>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Dreams.png')"></div>
      <p class="content">ចំនង់​និងការប្ដេជ្ញាចិត្តដ៏រឹងមាំ</p>
    </div>

    <div class="item">
      <div class="icon" style="background-image: url('/images/Solution.png')"></div>
      <p class="content">ជំនាញសហគ្រិនភាព​</p>
    </div>
  </div>
</div>

<div class="ready-banner cover" style="background-image: url('/images/Are-You-Ready.jpg')">
  <div class="banner sim green">
    <p class="title">
      តើអ្នកត្រៀមខ្លួនក្លាយ ទៅជាអ្នកច្នៃប្រឌិតវ័យក្មេង ជាមួយស្មាតហើយឬនៅ?
    </p>
    <p class="description">សូមបំពេញពាក្យចូលរួមតាមអ៊ីនធឺណែត៖</p>
    <div class="btn apply-now" onclick="renderPage('apply')">ដាក់ពាក្យឥលូវនេះ</div>
  </div>
</div>

<div class="sponsor">
  <p class="title callout yellow">INNOVATING WITH YOU, INVESTING IN YOU!</p>
  <div class="content">
    <div class="section">
      <!-- <p class="title">Endorsed by:</p> -->
      <div class="logos">
        <div class="logo" style="background-image: url('/images/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/CBRD.png')"></div>
      </div>
    </div>
    <div class="section">
      <!-- <p class="title">In partnership with:</p> -->
      <div class="logo" style="background-image: url('/images/Impact.png')"></div>
    </div>
    <div class="section">
      <!-- <p class="title">Powered by:</p> -->
      <div class="logo smart" style="background-image: url('/images/Smart.png')"></div>
    </div>
  </div>

  <p class="follow">តាមដានពួកយើងតាមរយៈ៖</p>
  <div class="icons">
    <div class="btn icon" style="background-image: url('/images/Facebook.png')"
      onclick="window.open('https://web.facebook.com/smartforcambodia/')"></div>
    <div class="btn icon" style="background-image: url('/images/Insta.png')"
      onclick="window.open('https://www.instagram.com/smartaxiata/')"></div>
    <div class="btn icon" style="background-image: url('/images/YouTube.png')"
      onclick="window.open('https://www.youtube.com/user/SmartAxiata')"></div>
  </div>
</div>
@endsection
