<div class="navbar">
  <div class="logo" style="background-image: url('/images/SmartStart.png')"
  onclick="window.location = '/kh'">
  </div>


  <div class="menu-web">
    <div class="btn item" onclick="window.location = '/kh/apply-now'">ដាក់ពាក្យចូលរួម</div>
    <div class="btn item" onclick="window.location = '/kh/faq'">សំណួរ និងចម្លើយ</div>
    <div class="btn item" onclick="window.location = '/kh/previous-winners'">អតីតជ័យលាភី</div>
    <div class="btn item" onclick="window.location = '/kh/contact-us'">ទាក់ទងពួកយើង</div>
    <div class="language">
      <p class="current">KH</p>
      <div class="dropdown">
        <p class="item" onclick="window.location = '/{{substr(Request::path(), 3)}}'">EN</p>
      </div>
    </div>
  </div>
</div>

<div class="spacer"></div>

<div class="menu-mobile">
  <div class="btn item" onclick="window.location = '/apply-now'">ដាក់ពាក្យចូលរួម</div>
  <div class="btn item" onclick="window.location = '/faq'">សំណួរ និងចម្លើយ</div>
  <div class="btn item" onclick="window.location = '/previous-winners'">អតីតជ័យលាភី</div>
  <div class="btn item" onclick="window.location = '/contact-us'">ទាក់ទងពួកយើង</div>
  <div id='language-kh' class="language" onclick="toggleLanguageDropdown()">
    <p class="current">KH</p>
    <div class="dropdown">
      <p class="item" onclick="renderLanguage('english')">EN</p>
    </div>
  </div>
</div>
