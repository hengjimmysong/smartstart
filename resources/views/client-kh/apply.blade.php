@extends('layouts.main-kh')
@section('header')
<title>ដាក់ពាក្យចូលរួម</title>
@endsection

@section('content')
<div class="apply-banner cover" style="background-image: url('/images/apply.jpg')">
  <div class="banner sim green">
    <div class="title">
      ដាក់ពាក្យចូលរួម
      <!-- <p class="subtitle">Get in touch today</p> -->
    </div>
  </div>
</div>

<div class="apply">
  <div class="content">
    <p class="title">តើអ្នកចង់ក្លាយជាសហគ្រិនបច្ចេកវិទ្យាឬ? </p>
    <p class="text">
    SmartStart គឺជាកម្មវិធីរយៈពេលប្រាំបួនខែដែលនឹងជួយបង្វែរគំនិតរបស់អ្នក ឲ្យក្លាយជាអាជីវកម្មទើបនឹងចាប់ផ្តើមថ្មីជោគជ័យបាន។ប្រសិនបើអ្នកគិតថាអ្នកអាចក្លាយជាសហគ្រិនវ័យក្មេងនោះ 
      <span class="highlight"  onclick="window.open('https://impacthubphnompenh.typeform.com/to/vPr1As')">សូមចុចនៅទីនេះ</span>
      ដើម្បីទទួលបទពិសោធន៍ផ្លាស់ប្តូរជីវិតរបស់អ្នក!
      <br><br>
      ដើម្បីមើលឯកសារអនឡាញជាមុន
      <a class="highlight" href="/resources/SmartStart_3_Application_Form.docx" download>ទាញយកនៅទីនេះ</a>។
      យើងទទួលយកពាក្យដែលដាក់តាមអនឡាញតែប៉ុណ្ណោះ។<br>
      ដើម្បីទទួលបានជំនួយក្នុងការបង្កើតឡើងនូវការច្នៃប្រឌិតបច្ចេកវិទ្យាដ៏ធំ សូមប្រើ
      <a class="highlight" href="/resources/SmartStart-Inspiration.pdf" download>គំរូសាមញ្ញ</a> មួយនេះ។
    </p>
  </div>

  <div class="image" style="background-image: url('/images/Apply.png')"></div>
</div>
@endsection
