@extends('layouts.main-kh')
@section('header')
<title>ទំនាក់ទំនងមកកាន់យើង</title>
@endsection

@section('content')
<div class="contact-banner cover" style="background-image: url('/images/Contact-us.jpg')">
  <div class="banner sim green">
    <div class="title">
      ទំនាក់ទំនងមកកាន់យើង
      <!-- <p class="subtitle">Get in touch today.</p> -->
    </div>
  </div>
</div>

<div class="contact">
  <div class="contact-info">
    <p class="title callout green">ទំនាក់ទំនងមកកាន់យើង</p>

    <div class="info">
      <div class="icon" style="background-image: url('/images/Phone.png')"></div>
      <div class="content">
        <p class="title">លេខទូស័ព្ទ</p>
        <p class="subtitle">+85510234075</p>
      </div>
    </div>

    <div class="info">
      <div class="icon" style="background-image: url('/images/Email.png')"></div>
      <div class="content">
        <p class="title">អ៊ីម៉ែល</p>
        <p class="subtitle">smartstart@smart.com.kh</p>
      </div>
    </div>

    <div class="info">
      <div class="icon" style="background-image: url('/images/Website.png')"></div>
      <div class="content">
        <p class="title">គេហទំព័រ</p>
        <p class="subtitle">smartstart.com.kh</p>
      </div>
    </div>
  </div>

  <form class="form" method="post" action="">
    <p class="title callout green">ឬបំពេញទំរងខាងក្រោម</p>

    <input type="text" name="name" id="form-name-kh" placeholder="ឈ្នោះ" class="input">
    <input type="text" name="email" id="form-email-kh" placeholder="អ៊ីម៉ែល" class="input">
    <input type="text" name="phone" id="form-phone-kh" placeholder="លេខទូស័ព្ទ" class="input">
    <textarea name="message" id="form-message-kh" placeholder="Message" class="input area"></textarea>

    <!-- <input type="submit" class="btn submit" id="submit" value="Submit"> -->
    <div class="btn submit" id="submit" onclick="mailKH()">ដាក់ពាក្យ</div>
  </form>


  <div class="map-ctn">
    <p class="title callout green">អាស័យដ្ឋាន</p>
    <div class="map">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.197852927056!2d104.92135141494555!3d11.537660347835356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310950dbd81ec78d%3A0x8d3ccf621a2b4e1f!2sSmart+Axiata!5e0!3m2!1sen!2skh!4v1552488128259"
        frameborder="0" style="border:0" allowfullscreen class="frame"></iframe>
      <div class="info"></div>
    </div>
  </div>


  <div class="social">
    <p class="follow">តាមដានពួកយើងតាមរយៈ៖</p>
    <div class="icons">
      <div class="btn icon" style="background-image: url('/images/Facebook.png')"
        onclick="window.open('https://web.facebook.com/smartforcambodia/')"></div>
      <div class="btn icon" style="background-image: url('/images/Insta.png')"
        onclick="window.open('https://www.instagram.com/smartaxiata/')"></div>
      <div class="btn icon" style="background-image: url('/images/YouTube.png')"
        onclick="window.open('https://www.youtube.com/user/SmartAxiata')"></div>
    </div>
  </div>
</div>
@endsection
