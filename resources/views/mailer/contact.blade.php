<style>
  * {
    font-family: 'Ubuntu', sans-serif;
  }
</style>

<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

<h1 style="color: #009a3e">New Contact Submission for SmartStart Website!</h1>

<span>There have been a new contact submission on SmartStart Website. To view all contacts,<a
    href="http://www.smartstart.com.kh/admin/contacts" style="color: #009a3e"> click here.</a></span>

<h2>Contact Details</h4>
<p>Name: {{$contact->name}}</p>
<p>Email: {{$contact->email}}</p>
<p>Phone Number: {{$contact->phone}}</p>
<p>Message: {{$contact->message}}</p>
